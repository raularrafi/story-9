from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import register
from django.http import HttpRequest

class UnitTest(TestCase):

	def test_homepage_url_is_exist(self):
		response = Client().get('/register/')
		self.assertEqual(response.status_code, 200)

	def test_login_url_is_exist(self):
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)

	def test_logout_url_is_exist(self):
		response = Client().get('/logout/')
		self.assertEqual(response.status_code, 200)

	def test_notexist_url_is_notexist(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code, 404)

	def test_homepage_using_homepage_function(self):
		response = resolve('/register/')
		self.assertEqual(response.func, register)

	def test_register_using_register_template(self):
		response = Client().get('/register/')
		self.assertTemplateUsed(response, 'homepage/register.html')

	def test_login_using_login_template(self):
		response = Client().get('/login/')
		self.assertTemplateUsed(response, 'homepage/login.html')

	def test_logout_using_logout_template(self):
		response = Client().get('/logout/')
		self.assertTemplateUsed(response, 'homepage/logout.html')

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
import time
from time import sleep

class FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium = webdriver.Chrome(chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(FunctionalTest, self).tearDown()

	def test_functional_page(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/register/')

		self.assertIn("Join Now",selenium.page_source)
		sleep(5)

		regisName = selenium.find_element_by_id("id_username")
		regisName.send_keys("ad3")
		sleep(15)
		pass1 = selenium.find_element_by_id("id_password1")
		pass1.send_keys("story9ppw")
		sleep(5)
		pass2 = selenium.find_element_by_id("id_password2")
		pass2.send_keys("story9ppw")
		sleep(5)
		sub = selenium.find_element_by_id("daftar")
		sub.click()
		sleep(10)
		self.assertIn("Log In", selenium.page_source)

	def test_login_functional(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000')

		self.assertIn("Log In",selenium.page_source)
		sleep(5)

		username = selenium.find_element_by_id("id_username")
		username.send_keys("user1")
		sleep(5)
		password = selenium.find_element_by_id("id_password")
		password.send_keys("story9ppw")
		sleep(5)
		submit = selenium.find_element_by_id("masuk")
		submit.click()
		sleep(10)
		self.assertIn("Welcome user1!",selenium.page_source)
