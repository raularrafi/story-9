from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required

def register(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)

		if form.is_valid():
			form.save()
			username = form.cleaned_data['username']
			password = form.cleaned_data['password1']
			user = authenticate(username=username, password=password)
			login(request, user)
			request.session["user"] = request.POST["username"]
			return redirect('login')

	else:
		form = UserCreationForm()

	context = {'form' : form}
	return render(request, 'homepage/register.html', context)

@login_required
def index(request):
    username = request.session.get("user")
    return render(request, "homepage/index.html", {"username":username})

